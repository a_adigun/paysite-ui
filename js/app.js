var app = angular.module('app', ['ui.router']);
app.config(function($stateProvider, $urlRouterProvider){
  $stateProvider
    .state('home', {
      url:'/home',
      templateUrl:'tpl/home.html',
      controller:'baseCon',
      controllerAs:'vm'
    })
    .state('pricing', {
      url:'/pricing',
      templateUrl:'tpl/pricing.html',
      controller:'baseCon',
      controllerAs:'vm'
    })
    .state('signup', {
      url:'/signup',
      templateUrl:'tpl/signup.html'
    })
    .state('contact',{
      url:'/contact',
      templateUrl:'tpl/contact.html'
    })
    .state('rec-funds', {
      url:'/receive',
      templateUrl:'tpl/pdt/rec-funds.html'
    })
    .state('transfer', {
      url:'/transfer',
      templateUrl:'tpl/pdt/transfer.html'
    })
    .state('bill', {
      url:'/bill',
      templateUrl:'tpl/pdt/bill.html'
    })
    .state('airtime', {
      url:'/airtime',
      templateUrl:'tpl/pdt/airtime.html'
    })
    .state('hotel', {
      url:'/hotel',
      templateUrl:'tpl/pdt/hotel.html'
    })
    .state('movie', {
      url:'/movie',
      templateUrl:'tpl/pdt/movie.html'
    })
    .state('atlas', {
      url:'/atlas',
      templateUrl:'tpl/about/atlas.html'
    })
    .state('initiatives', {
      url:'/initiatives',
      templateUrl:'tpl/about/initiatives.html'
    })
    .state('team', {
      url:'/team',
      templateUrl:'tpl/about/team.html'
    })
    .state('api', {
      url:'/api',
      templateUrl:'tpl/dev/api.html'
    })
    .state('usps', {
      url:'/usps',
      templateUrl:'tpl/dev/usps.html'
    })
    .state('isw', {
      url:'/isw',
      templateUrl:'tpl/dev/isw.html'
    })
    ;
    $urlRouterProvider.otherwise('/home');
})
