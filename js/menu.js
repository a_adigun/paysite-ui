$(document).on('pagebeforecreate', '#index', function(){
    $('[data-role="navbar"]').html('<ul>' +
        '<li><a href="#SomePage" data-transition="fade" data-icon="none">By Brand</a></li>' +
        '<li><a href="#AnotherPage" data-transition="fade" data-icon="none">By Flavor</a></li>' +
        '<li><a href="#LastPage" data-transition="fade" data-icon="none">Zero Nicotine</a></li>' +
        '</ul>');
});
